<?php

namespace Drupal\dialog_tools\Plugin\views\field;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\EntityOperations as CoreEntityOperationsField;
use Drupal\views\ResultRow;

/**
 * Extends the Content Moderation State field handler with additional options.
 */
class EntityOperationsField extends CoreEntityOperationsField {

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();

    $options['dialog_enabled'] = [
      'default' => FALSE,
    ];

    $options['dialog_enabled_links'] = [
      'default' => '',
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['dialog_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open links in modal'),
      '#default_value' => $this->options['dialog_enabled'],
    ];

    $form['dialog_enabled_links'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Modal enabled links'),
      '#description' => $this->t('List of operation link IDs to be open in a modal (eg. edit) separated by comma. Leave empty to apply to all.'),
      '#default_value' => $this->options['dialog_enabled_links'],
      '#states' => [
        'visible' => [
          ':input[name="options[dialog_enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $build = parent::render($values);
    if (is_array($build) && $this->options['dialog_enabled']) {
      $build['#attached']['library'][] = 'core/drupal.ajax';

      $enabled_links = $this->options['dialog_enabled_links']
        ? explode(',', $this->options['dialog_enabled_links'])
        : [];
      foreach ($build['#links'] as $id => &$link) {
        if (!$enabled_links || array_search($id, $enabled_links) !== FALSE) {
          $link['attributes']['class'][] = 'use-ajax';
          $link['attributes']['data-dialog-type'] = 'modal';
          $link['attributes']['data-dialog-options'] = Json::encode([
            'width' => '80%',
          ]);
        }
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    if (!empty($this->options['dialog_enabled'])) {
      // Adds Dialog Tools itself to the views module dependencies if enabled.
      $dependencies['module'][] = 'dialog_tools';
    }

    return $dependencies;
  }

}
