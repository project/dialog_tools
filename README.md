# Dialog Tools Drupal module

## Contents

- [Contents](#contents)
- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
  - [From a render array](#from-a-render-array)
  - [From views](#from-views)
- [Contact](#contact)

## Introduction

The Dialog Tools module provides a set of utilities that extends the [dialog
support](https://www.drupal.org/node/1852224) present in Drupal 9+.

The primary use case for this module is to:

- make it easy to display contents in an interactive overlay
- extend existing features with a display in modal option

## Requirements

This module requires no modules outside of Drupal core.

## Installation

The installation of this module is like other Drupal modules:

1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
   use Composer to download the Dialog Tools module running
   ```composer require "drupal/dialog_tools"```. Otherwise copy/upload the
   module code to the modules directory of your Drupal installation.

2. Enable the Dialog Tools module in Extend (`/admin/modules`).

3. Set up user permissions. (`/admin/people/permissions#module-webform`)

## Configuration

Currently there is no configuration options.

## Usage

### From a render array

You can display content in dialogs from render arrays in a declarative way.

1. Attach the library dialog_tools/discover to your render array

   ```php
   $build['#attached']['library'][] = 'dialog_tools/discover';
   ```

2. Create the modal content, eg. in a container with the class name "dialog-tools-dialog"

   ```php
   use Drupal\Component\Serialization\Json;
   ```

   ```php
   $build['dialog']['#type'] = 'container';
   // The "dialog-tools-dialog" class makes the container being discovered and
   // initialized by Dialog Tools.
   $build['dialog']['#attributes']['class'][] = 'dialog-tools-dialog';
   // The "data-dialog-tools-button" attribute contains the ID of the opening
   // button element.
   $build['dialog']['#attributes']['data-dialog-tools-button'] = 'modal-open-button';
   // Some dialog options, see more at https://api.jqueryui.com/dialog/
   $build['dialog']['#attributes']['data-dialog-tools-options'] = Json::encode([
     'title' => t('Modal title'),
     'width' => '80%',
     'modal' => TRUE,
   ]);

   $build['dialog']['content'] = [
     '#markup' => t('Test!'),
   ];
   ```

3. Add the modal opening button

   ```php
   $build['modal_open_button'] = [
     '#type' => 'html_tag',
     '#tag' => 'button',
     '#value' => t('Open modal'),
     '#attributes' => [
       'type' => 'button',
       'id' => 'modal-open-button',
       'class' => ['button', 'button--primary'],
     ],
   ];
   ```

### From views

Currently Dialog Tools enhace the operations links views field with the ability
to load and display link target content in a modal dialog. To do so, just enable
de option "Open links in modal" in the operations field setting. It can be
enabled only to some particular links by entering their IDs in an additional
configuration option.

## Contact

Current maintainers:

- Manuel Adan (manuel.adan) - https://www.drupal.org/u/manueladan

This project has the following supporting organizations:

- NTT DATA - https://www.nttdata.com/
