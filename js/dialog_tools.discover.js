Drupal.behaviors.dialog_tools_discover = {
  attach: function attach(context, settings) {
    once('dialog_tools', '.dialog-tools-dialog', context).forEach(
      function (element) {
        var options = JSON.parse(element.getAttribute('data-dialog-tools-options') ?? '{}');
        dialog = Drupal.dialog(element, options);

        // Initializes the dialog by opening and closing it.
        /** @todo Do it in a properly way. */
        dialog.show();
        // Do not close the dialog if contains form field elements with errors.
        if (!element.querySelector('label.has-error')) {
          dialog.close();
        }

        // Set an event listener on the activation button.
        dialog.is_modal = options.modal;
        var button = document.getElementById(element.getAttribute('data-dialog-tools-button'));
        if (button) {
          button.dialog = dialog;
          button.addEventListener('click', function (e) {
            e.target.dialog.is_modal ? e.target.dialog.showModal() : e.target.dialog.show();
          });
        }
      }
    )
  }
}
